CREATE DATABASE test; 

USE test;

--
-- Base de données : `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `authtoken_token`
--

INSERT INTO `authtoken_token` (`key`, `created`, `user_id`) VALUES
('0ecbb2327e5eda8f839101185d61b64ff89ea52d', '2022-12-06 12:02:20.486225', 3),
('280e11b8746d14f6e6cfddcecf095abbdc468fb0', '2022-12-07 10:54:51.162647', 4),
('653d81da73f1ec3ad9e3cb2329cb91644b33c62e', '2022-12-07 10:55:06.302470', 5),
('6b21e57c70b644dba45e09bd7c7ea5bba72225bd', '2022-12-06 11:51:07.234531', 2);

-- --------------------------------------------------------

--
-- Structure de la table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Supervision');

-- --------------------------------------------------------

--
-- Structure de la table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint NOT NULL,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `auth_group_permissions`
--

INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 9),
(2, 1, 10),
(3, 1, 11),
(4, 1, 12),
(5, 1, 13),
(6, 1, 14),
(7, 1, 15),
(8, 1, 16),
(9, 1, 18),
(10, 1, 20),
(13, 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add client', 1, 'add_client'),
(2, 'Can change client', 1, 'change_client'),
(3, 'Can delete client', 1, 'delete_client'),
(4, 'Can view client', 1, 'view_client'),
(5, 'Can add log entry', 2, 'add_logentry'),
(6, 'Can change log entry', 2, 'change_logentry'),
(7, 'Can delete log entry', 2, 'delete_logentry'),
(8, 'Can view log entry', 2, 'view_logentry'),
(9, 'Can add permission', 3, 'add_permission'),
(10, 'Can change permission', 3, 'change_permission'),
(11, 'Can delete permission', 3, 'delete_permission'),
(12, 'Can view permission', 3, 'view_permission'),
(13, 'Can add group', 4, 'add_group'),
(14, 'Can change group', 4, 'change_group'),
(15, 'Can delete group', 4, 'delete_group'),
(16, 'Can view group', 4, 'view_group'),
(17, 'Can add user', 5, 'add_user'),
(18, 'Can change user', 5, 'change_user'),
(19, 'Can delete user', 5, 'delete_user'),
(20, 'Can view user', 5, 'view_user'),
(21, 'Can add content type', 6, 'add_contenttype'),
(22, 'Can change content type', 6, 'change_contenttype'),
(23, 'Can delete content type', 6, 'delete_contenttype'),
(24, 'Can view content type', 6, 'view_contenttype'),
(25, 'Can add session', 7, 'add_session'),
(26, 'Can change session', 7, 'change_session'),
(27, 'Can delete session', 7, 'delete_session'),
(28, 'Can view session', 7, 'view_session'),
(29, 'Can add Token', 8, 'add_token'),
(30, 'Can change Token', 8, 'change_token'),
(31, 'Can delete Token', 8, 'delete_token'),
(32, 'Can view Token', 8, 'view_token'),
(33, 'Can add token', 9, 'add_tokenproxy'),
(34, 'Can change token', 9, 'change_tokenproxy'),
(35, 'Can delete token', 9, 'delete_tokenproxy'),
(36, 'Can view token', 9, 'view_tokenproxy'),
(37, 'can change statut client', 1, 'statut_client'),
(38, 'Can add employe', 10, 'add_employe'),
(39, 'Can change employe', 10, 'change_employe'),
(40, 'Can delete employe', 10, 'delete_employe'),
(41, 'Can view employe', 10, 'view_employe');

-- --------------------------------------------------------

--
-- Structure de la table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$390000$Rw1pGGWELOmVZnXF8pQKNN$Rgaz7aPsxOrmpdE1k+a/zOui+oBpfvgsMgqY+d6u6OY=', NULL, 1, 'superuser', '', '', '', 1, 1, '2022-12-03 13:58:34.717399'),
(2, 'pbkdf2_sha256$390000$Hw11BR9b3o6MnJd7J36hYq$Syn1KjyQhA9TdTTRPL3OD+jXj3bjmIhWypsZv1AkhgA=', '2022-12-07 20:47:51.731248', 1, 'admin_root', '', '', '', 1, 1, '2022-12-03 14:00:08.159595'),
(3, 'pbkdf2_sha256$390000$SgpPLJ8cZ1S7SfbDTEu2Md$joS3qyf908nkZjD6OkWYAeO+wK8IilPJTO+tkwh5Adg=', '2022-12-03 18:33:52.202337', 0, 'user_1', '', '', '', 1, 1, '2022-12-03 16:44:37.000000'),
(4, 'pbkdf2_sha256$390000$yn9XMxpwZJzEpHb5H5JURA$fMye7I9pTNZbAhNJMqKwTUyZi8o3qSPmpaGH8Zzrtj0=', '2022-12-07 10:49:59.101107', 0, 'user_2', '', '', '', 1, 1, '2022-12-03 16:44:59.000000'),
(5, 'pbkdf2_sha256$390000$PnhdXJAsXgY5bzi7Gz5mCB$4sMJd+mopZN4fdvNAum0p9+MjtONGnmpUbMrmjiVmJQ=', '2022-12-07 20:47:18.000000', 0, 'user_3', '', '', '', 1, 1, '2022-12-03 16:45:10.000000');

-- --------------------------------------------------------

--
-- Structure de la table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` bigint NOT NULL,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `auth_user_groups`
--

INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 3, 1),
(2, 4, 2),
(4, 5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` bigint NOT NULL,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `client_client`
--

CREATE TABLE `client_client` (
  `id` bigint NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `create_at` date NOT NULL,
  `update_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `client_client`
--

INSERT INTO `client_client` (`id`, `name`, `surname`, `statut`, `create_at`, `update_at`) VALUES
(1, 'Ngounou Takeu', 'Herve4', 0, '2022-12-05', '2022-12-07 11:02:24.533802'),
(2, 'Juniore', 'Kamdem', 1, '2022-12-05', '2022-12-07 06:21:15.960958'),
(3, 'Christine', 'Tientcheu', 1, '2022-12-05', '2022-12-07 06:18:10.389781'),
(4, 'Meggane', 'Sukam', 0, '2022-12-07', '2022-12-07 06:21:25.772275');

-- --------------------------------------------------------

--
-- Structure de la table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL
) ;

--
-- Déchargement des données de la table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2022-12-03 16:44:38.033030', '3', 'user_1', 1, '[{\"added\": {}}]', 5, 2),
(2, '2022-12-03 16:44:59.540700', '4', 'user_2', 1, '[{\"added\": {}}]', 5, 2),
(3, '2022-12-03 16:45:11.263518', '5', 'user_3', 1, '[{\"added\": {}}]', 5, 2),
(4, '2022-12-03 16:48:16.098844', '3', 'user_1', 2, '[{\"changed\": {\"fields\": [\"Active\"]}}]', 5, 2),
(5, '2022-12-03 16:53:59.304240', '1', 'Admin', 1, '[{\"added\": {}}]', 4, 2),
(6, '2022-12-03 16:54:16.047784', '3', 'user_1', 2, '[{\"changed\": {\"fields\": [\"Active\", \"Staff status\", \"Groups\"]}}]', 5, 2),
(7, '2022-12-03 18:31:48.588566', '2', 'Supervision', 1, '[{\"added\": {}}]', 4, 3),
(8, '2022-12-03 18:32:31.954277', '4', 'user_2', 2, '[{\"changed\": {\"fields\": [\"Staff status\", \"Groups\"]}}]', 5, 3),
(9, '2022-12-07 19:29:23.284234', '2', 'Supervision', 2, '[{\"changed\": {\"fields\": [\"Permissions\"]}}]', 4, 2),
(10, '2022-12-07 20:45:12.324386', '5', 'user_3', 2, '[{\"changed\": {\"fields\": [\"Staff status\"]}}]', 5, 2),
(11, '2022-12-07 20:46:41.650980', '5', 'user_3', 2, '[{\"changed\": {\"fields\": [\"Groups\"]}}]', 5, 2),
(12, '2022-12-07 20:47:04.229594', '2', 'Supervision', 2, '[]', 4, 2),
(13, '2022-12-07 20:49:35.772191', '2', 'Supervision', 2, '[{\"changed\": {\"fields\": [\"Permissions\"]}}]', 4, 2),
(14, '2022-12-08 06:31:24.810013', '5', 'user_3', 2, '[{\"changed\": {\"fields\": [\"Groups\"]}}]', 5, 2),
(15, '2022-12-08 11:19:09.773096', '5', 'user_3', 2, '[{\"changed\": {\"fields\": [\"Groups\"]}}]', 5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(2, 'admin', 'logentry'),
(4, 'auth', 'group'),
(3, 'auth', 'permission'),
(5, 'auth', 'user'),
(8, 'authtoken', 'token'),
(9, 'authtoken', 'tokenproxy'),
(1, 'client', 'client'),
(6, 'contenttypes', 'contenttype'),
(10, 'employe', 'employe'),
(7, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Structure de la table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-12-03 03:26:55.074320'),
(2, 'auth', '0001_initial', '2022-12-03 03:26:56.763013'),
(3, 'admin', '0001_initial', '2022-12-03 03:26:57.155961'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-12-03 03:26:57.183442'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-12-03 03:26:57.207191'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-12-03 03:26:57.411270'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-12-03 03:26:57.617919'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-12-03 03:26:57.683801'),
(9, 'auth', '0004_alter_user_username_opts', '2022-12-03 03:26:57.710990'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-12-03 03:26:57.846117'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-12-03 03:26:57.857701'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-12-03 03:26:57.879637'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-12-03 03:26:58.044805'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-12-03 03:26:58.201378'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-12-03 03:26:58.246154'),
(16, 'auth', '0011_update_proxy_permissions', '2022-12-03 03:26:58.266002'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-12-03 03:26:58.424196'),
(18, 'client', '0001_initial', '2022-12-03 03:26:58.489355'),
(19, 'client', '0002_rename_date_client_create_at_client_update_at', '2022-12-03 03:26:58.593107'),
(20, 'client', '0003_alter_client_update_at', '2022-12-03 03:26:58.710802'),
(21, 'sessions', '0001_initial', '2022-12-03 03:26:58.818769'),
(22, 'authtoken', '0001_initial', '2022-12-06 01:39:05.467559'),
(23, 'authtoken', '0002_auto_20160226_1747', '2022-12-06 01:39:05.566260'),
(24, 'authtoken', '0003_tokenproxy', '2022-12-06 01:39:05.579037'),
(25, 'employe', '0001_initial', '2022-12-08 15:45:41.781524');

-- --------------------------------------------------------

--
-- Structure de la table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('5i40hng9d1x1365izb4dx524vewk3x2m', '.eJxVjDsOwjAQBe_iGln-LoaSnjNYu_YaB5AtxUmFuDtESgHtm5n3EhHXpcZ18BynLM7CisPvRpge3DaQ79huXabelnkiuSlyp0Nee-bnZXf_DiqO-q1LBjYQnDG6gCNvyDuyUIhUQfYZUQUbiiZCNAp88Vl7ODKHcErasXh_APNUOFw:1p1XKi:pfqV5I-J3szkc3UGcWr9X6S0AIwg-Tx3ZRMu9aGLoBQ', '2022-12-17 18:33:52.212525'),
('if6cpuqgpsqxnsvjd7wxqylpz5puk8wd', '.eJxVjMsOwiAQRf-FtSEwPFpcuvcbyAwMUjU0Ke3K-O_apAvd3nPOfYmI21rj1nmJUxZnAeL0uxGmB7cd5Du22yzT3NZlIrkr8qBdXufMz8vh_h1U7PVbe7KBrFYEWDzjwNoQaaNG63IoZbDaOeNReYLAaACSUkzeqjEDe2PF-wPa3DdX:1p31KZ:pWw9tODltbZsKoCFryDC0T-kGC5zj0LxRarAiysofI8', '2022-12-21 20:47:51.754873');

-- --------------------------------------------------------

--
-- Structure de la table `employe_employe`
--

CREATE TABLE `employe_employe` (
  `id` bigint NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `update_at` datetime(6) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `employe_employe`
--

INSERT INTO `employe_employe` (`id`, `name`, `surname`, `statut`, `update_at`, `create_at`) VALUES
(1, 'Employe N1', 'prenom 1', 1, '2022-12-08 16:01:07.597321', '2022-12-08');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Index pour la table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Index pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Index pour la table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Index pour la table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Index pour la table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Index pour la table `client_client`
--
ALTER TABLE `client_client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Index pour la table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Index pour la table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Index pour la table `employe_employe`
--
ALTER TABLE `employe_employe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT pour la table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `client_client`
--
ALTER TABLE `client_client`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `employe_employe`
--
ALTER TABLE `employe_employe`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Contraintes pour la table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Contraintes pour la table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Contraintes pour la table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Contraintes pour la table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Contraintes pour la table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;