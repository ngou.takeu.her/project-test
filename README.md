<!-- Installation -->

<!-- Vous avez besoin de cmake pour plus de facilite (optionel) -->

# cas d'utilisation avec cmake

# list des commande taper la commande:

make

<!-- sans cmake -->

# build

docker-compose ---project-name test build

# up

docker-compose --project-name test up -d

# logs Api

docker logs -f $$(docker-compose --project-name test -f docker-compose.yml ps -q api)

# attendre que les application se lance

<!-- en cas d'erreur de l'application frontend excuter npm install et npm start -->
