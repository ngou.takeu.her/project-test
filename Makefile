ROOT_DIR       := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL          := $(shell which bash)
PROJECT_NAME     = test
ARGS            = $(filter-out $@,$(MAKECMDGOALS))

.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell
default: help-default;   # default target
Makefile: ;              # skip prerequisite discovery

help-default:
	@echo "                          ====================================================================="
	@echo "                          Aide & Menu"
	@echo "                          ====================================================================="
	@echo ""
	@echo "                   help: Affiche menu aide"
	@echo "                   status: Status des containers"
	@echo ""
	@echo "                          ====================================================================="
	@echo "                          Main Menu"
	@echo "                          ====================================================================="
	@echo "                   up: Creer et demarer l'application en mode detache (in the background)"
	@echo "                   stop: Arette l'application"
	@echo "                   build: Build ou rebuild les services"
	@echo "                   logs: Affiche les logs"
	@echo "                   down: Supprime les containers creer"
	@echo "                   exec-api: Execute les commandes dans le containers api"
	@echo "                   migrate: Appliquer les migrations"
	@echo "                   module: Creer une application (parme name='name application')"


build:
	docker-compose --project-name $(PROJECT_NAME) build
up:
	docker-compose --project-name $(PROJECT_NAME) up -d
stop:
	docker-compose --project-name $(PROJECT_NAME) stop
logs:
	docker logs -f $$(docker-compose --project-name $(PROJECT_NAME) -f docker-compose.yml ps -q api)
status:
	docker-compose --project-name $(PROJECT_NAME) ps
down:
	docker-compose --project-name $(PROJECT_NAME) down
module:
	docker exec -it --workdir /src/src -u root $$(docker-compose --project-name $(PROJECT_NAME) ps -q api) python manage.py startapp $(name)
exec-api:
	docker exec -it --workdir /src/src -u root $$(docker-compose --project-name $(PROJECT_NAME) ps -q api) /bin/sh
migrate:
	docker exec -it --workdir /src/src -u root $$(docker-compose --project-name $(PROJECT_NAME) ps -q api) python manage.py migrate
migrations:
	docker exec -it --workdir /src/src -u root $$(docker-compose --project-name $(PROJECT_NAME) ps -q api) python manage.py makemigrations $(name)