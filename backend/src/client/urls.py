from rest_framework import routers
from client.views import ClientViewSet

router = routers.DefaultRouter()

router.register('clients', ClientViewSet)

# from django.urls import path

# from client.views import ListClientView, DetailClientView


# urlpatterns = [
#     path('clients/', ListClientView.as_view()),
#     path('client/<int:pk>', DetailClientView.as_view())
# ]
