from django.urls import reverse_lazy
from rest_framework.test import APITestCase

from client.models import Client

# Create your tests here.
class ClientTestCase(APITestCase):

    url = reverse_lazy('/api/clients')

    def format_datetime(self, value):
        # Cette méthode est un helper permettant de formater une date en chaine de caractères sous le même format que celui de l’api
        return value.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    def test_list_client(self):
        client = Client.objects.create(name: "test", surname: "test1")
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        excepted = [
            {
                'id': client.pk,
                'name': client.name,
                'surname': client.surname,
                'create_at': self.format_datetime(client.create_at),
                'update_at': self.format_datetime(client.update_at),
                'statut': client.statut
            }
        ]
        self.assertEqual(excepted, response.json())

    
    def test_create_client(self):
        # voir le nombre d'element dans notre db
        # Ajouter un objet dans notre db
        # Valider que le nombre d'objets dans nore db a ete incremente de 1
        nb_clients_old = Client.objects.count()

        newClient = Client()
        newClient.name = "Ngounou Takeu"
        newClient.surname = "Herve"

        newClient.save()

        nb_client_new = Client.objects.cout()

        self.assertTrue( nb_clients_old + 1 == nb_client_new)