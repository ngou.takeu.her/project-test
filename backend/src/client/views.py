from rest_framework import viewsets
from client.permissions import CanChangeStatut
from client.serializers import ClientSerializer
from client.models import Client
from rest_framework.response import Response
# Create your views here.


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [CanChangeStatut]

    # def list(self, request, *args, **kwargs):
    #     queryset = self.filter_queryset(self.get_queryset())
    #     page = self.paginate_queryset(queryset)
    #     permissions.has_
    #     if page is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)

    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response

# class ListClientView(generics.ListCreateAPIView):
#     queryset = Client.objects.all()
#     serializer_class = ClientSerializer


# class DetailClientView(generics.RetrieveDestroyAPIView):
#     queryset = Client.objects.all()
#     serializer_class = ClientSerializer
