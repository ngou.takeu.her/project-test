from rest_framework import serializers
from client.models import Client


class ClientSerializer(serializers.ModelSerializer):

    # updateAt = serializers.DateTimeField(source='update_at')

    class Meta:
        model = Client
        # exclude = ['update_at']
        fields = '__all__'

# class ClientCreateSerializer(serializers)
