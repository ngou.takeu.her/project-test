from django.db import models

# Create your models here.


class Employe(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    statut = models.BooleanField(default=False)
    update_at = models.DateTimeField(auto_now=True)
    create_at = models.DateField(auto_now_add=True)
