from rest_framework import serializers
from employe.models import Employe


class EmployeSerializer(serializers.ModelSerializer):

    # updateAt = serializers.DateTimeField(source='update_at')

    class Meta:
        model = Employe
        # exclude = ['update_at']
        fields = '__all__'

# class EmployeCreateSerializer(serializers)
