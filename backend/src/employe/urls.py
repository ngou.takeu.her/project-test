from rest_framework import routers
from employe.views import EmployeViewSet

router = routers.DefaultRouter()

router.register('employes', EmployeViewSet)
