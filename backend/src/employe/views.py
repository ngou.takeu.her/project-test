from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from client.permissions import CanChangeStatut
from employe.serializers import EmployeSerializer
from employe.models import Employe
from rest_framework.response import Response
# Create your views here.


class EmployeViewSet(viewsets.ModelViewSet):
    queryset = Employe.objects.all()
    serializer_class = EmployeSerializer
    permission_classes = [CanChangeStatut]
