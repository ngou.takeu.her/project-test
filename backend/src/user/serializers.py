from rest_framework import serializers
from django.contrib.auth.models import User, Group, Permission


class PermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ['codename']


class GroupSerializer(serializers.ModelSerializer):
    permissions = PermissionsSerializer(many=True)

    class Meta:
        model = Group
        fields = ('name', 'permissions')


class userSerializers(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = '__all__'
