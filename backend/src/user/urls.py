from rest_framework import routers
from user.views import userviewsets

router = routers.DefaultRouter()

router.register('user', userviewsets, basename="user_api")
