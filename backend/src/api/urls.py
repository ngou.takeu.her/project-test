"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from user.views import UserLogin

from client.urls import router as client_router
from user.urls import router as user_router
from employe.urls import router as employe_router

router = routers.DefaultRouter()
router.registry.extend(client_router.registry)
router.registry.extend(user_router.registry)
router.registry.extend(employe_router.registry)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('login/', obtain_auth_token),
    path('auth/', UserLogin.as_view())
    # path('auth/', include(user_router.urls))
    # re_path('api/', include('client.urls'))
]
