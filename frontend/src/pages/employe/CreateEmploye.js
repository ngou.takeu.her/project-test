import MainCard from 'components/MainCard';
import EmployeModel from './ModelEmploye';

function CreateEmploye() {
    const defaultValues = {
        name: '',
        surname: ''
    };

    const method = {
        method: 'post',
        id: null
    };

    return (
        <MainCard>
            <EmployeModel defaultValues={defaultValues} method={method} />
        </MainCard>
    );
}

export default CreateEmploye;
