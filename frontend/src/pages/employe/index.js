import { UserAddOutlined } from '@ant-design/icons';
import { postPatchAxios } from 'components/axios';
import { loadAxios } from 'components/axios';
import Loader from 'components/Loader';
import MainCard from 'components/MainCard';
import { useCallback, useEffect, useState } from 'react';
import { useAuthHeader } from 'react-auth-kit';
import { useQuery } from 'react-query';
import { Link, redirect, useNavigate } from 'react-router-dom';
// import Typography from 'themes/overrides/Typography';
import OrderTableEmploye from './OrderTableEmploye';

import { Box, Typography, Button } from '@mui/material';

const Employe = () => {
    const authHeader = useAuthHeader();

    const navigate = useNavigate();
    const [onEdit, setOnEdit] = useState(false);

    const handleChangeOnEdit = useCallback((statut) => {
        if (statut !== 403) {
            setOnEdit(true);
        } else if (statut === 403) {
            navigate('/403');
        }
    });

    const _onEdit = () => {
        postPatchAxios('api/employes/0/', {}, authHeader(), 'patch', handleChangeOnEdit);
        return;
    };

    _onEdit();

    const { data, isLoading, isError } = useQuery(['employe_index'], () => loadAxios('api/employes/', authHeader(), navigate), {
        retry: 0
    });
    return (
        <Box>
            {isLoading ? (
                <Loader />
            ) : isError ? (
                <Box>
                    <Typography variant="h4">Error fecth data</Typography>
                </Box>
            ) : (
                data && (
                    <Box>
                        {onEdit && (
                            <Box mb={3}>
                                <Button component={Link} to="/employe/add" color="success">
                                    <UserAddOutlined />
                                    <Typography sx={{ ml: 1 }}>Ajouter un employe</Typography>
                                </Button>
                            </Box>
                        )}
                        <MainCard title="Liste des employes">
                            <OrderTableEmploye rows={data} />
                        </MainCard>
                    </Box>
                )
            )}
        </Box>
    );
};

export default Employe;
