import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
// material-ui
import {
    Box,
    Button,
    Icon,
    Link,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@mui/material';
// project import
import Dot from 'components/@extended/Dot';
import Moment from 'react-moment';
import { toast } from 'react-toastify';
import { postPatchAxios } from 'components/axios';
import { useAuthHeader } from 'react-auth-kit';
import { useQueryClient } from 'react-query';

// const createData = (id, name, status) => ({
//     id,
//     name,
//     status
// });

// const rows = [createData(13434, 'Ngounou takeu herve', 1), createData(1334434, 'Ngounou takeu herve', 1)];

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc' ? (a, b) => descendingComparator(a, b, orderBy) : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

// ==============================|| ORDER TABLE - HEADER CELL ||============================== //

const headCells = [
    {
        id: 'id',
        align: 'left',
        disablePadding: false,
        label: 'id'
    },
    {
        id: 'name',
        align: 'left',
        disablePadding: true,
        label: 'Nom et prenom'
    },
    {
        id: 'carbs',
        align: 'left',
        disablePadding: false,

        label: 'Status'
    },
    {
        id: 'date',
        align: 'center',
        disablePadding: false,

        label: 'Date creation'
    },
    {
        id: 'options',
        align: 'right',
        disablePadding: false,
        label: 'Options'
    }
];

// ==============================|| ORDER TABLE - HEADER ||============================== //

function OrderTableHead({ order, orderBy }) {
    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <TableCell
                        key={uuid()}
                        align={headCell.align}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

OrderTableHead.propTypes = {
    order: PropTypes.string,
    orderBy: PropTypes.string
};

// ==============================|| ORDER TABLE - STATUS ||============================== //

const OrderStatus = ({ status }) => {
    let color;
    let title;

    switch (status) {
        case 0:
            color = 'warning';
            title = 'Pending';
            break;
        case 1:
            color = 'success';
            title = 'Non suspendu';
            break;
        case 2:
            color = 'error';
            title = 'Suspendu';
            break;
        default:
            color = 'primary';
            title = 'None';
    }

    return (
        <Stack direction="row" spacing={1} alignItems="center">
            <Dot color={color} />
            <Typography>{title}</Typography>
        </Stack>
    );
};

OrderStatus.propTypes = {
    status: PropTypes.number
};

// ==============================|| ORDER TABLE ||============================== //

export default function OrderTableClient({ rows }) {
    const [order] = useState('asc');
    const [orderBy] = useState('id');
    const [selected] = useState([]);
    const [onEdit, setOnEdit] = useState(false);
    const [onEditStatut, setOnEditStatut] = useState(false);

    const token = useAuthHeader();
    const navigate = useNavigate();
    const queryClient = useQueryClient();

    const handleChangeOnEdit = useCallback((statut) => {
        console.log(statut);
        if (statut !== 403) {
            setOnEdit(true);
        }
    });

    const handleChangeOnEditStatut = useCallback((statut) => {
        console.log(statut);
        if (statut !== 403) {
            setOnEditStatut(true);
        }
    });

    const _onChangeStatut = async (id, status, name, callback) => {
        if (callback) {
            await postPatchAxios('api/employes/0/', {}, token(), 'patch', handleChangeOnEdit);
            await postPatchAxios('api/employe/0/', {}, token(), 'post', handleChangeOnEditStatut);
            return;
        }
        const response = await toast.promise(postPatchAxios(`api/employes/${id}/`, { statut: !status }, token(), 'patch'), {
            pending: 'Changement de statut en cours',
            success: `vous avez ${status ? 'bloque' : 'deploque'} ${name}`,
            error: 'Une erreur est survenue'
        });

        if (response.isSuccess) {
            console.log(response.data);
            queryClient.setQueryData(['employe_index'], (employes) => {
                return employes.map((c) => (c.id === response.data.id ? response.data : c));
            });
        }
    };

    _onChangeStatut(0, false, '', true);

    const isSelected = (id) => selected.indexOf(id) !== -1;
    return (
        <Box>
            <TableContainer
                sx={{
                    width: '100%',
                    overflowX: 'auto',
                    position: 'relative',
                    display: 'block',
                    maxWidth: '100%',
                    '& td, & th': { whiteSpace: 'nowrap' }
                }}
            >
                <Table
                    aria-labelledby="tableTitle"
                    sx={{
                        '& .MuiTableCell-root:first-child': {
                            pl: 2
                        },
                        '& .MuiTableCell-root:last-child': {
                            pr: 3
                        }
                    }}
                >
                    <OrderTableHead order={order} orderBy={orderBy} />
                    <TableBody>
                        {stableSort(rows, getComparator(order, orderBy)).map((row, index) => {
                            const isItemSelected = isSelected(row.id);
                            const labelId = `enhanced-table-checkbox-${index}`;

                            return (
                                <TableRow
                                    hover
                                    role="checkbox"
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    aria-checked={isItemSelected}
                                    tabIndex={-1}
                                    key={row.id}
                                    selected={isItemSelected}
                                >
                                    <TableCell component="th" id={labelId} scope="row" align="left">
                                        <Link color="secondary" component={RouterLink} to="">
                                            {row.id}
                                        </Link>
                                    </TableCell>
                                    <TableCell align="left">{`${row.name} ${row.surname}`}</TableCell>
                                    <TableCell align="left">
                                        <Box display="flex">
                                            <OrderStatus status={row.statut ? 1 : 2} />
                                            {onEditStatut && (
                                                <Button
                                                    onClick={() => _onChangeStatut(row.id, row.statut, `${row.surname} ${row.name}`)}
                                                    color="info"
                                                >
                                                    <Icon>loop</Icon>
                                                </Button>
                                            )}
                                        </Box>
                                    </TableCell>
                                    <TableCell align="center">
                                        <Moment format="D MMM YYYY">{row.createAt}</Moment>
                                    </TableCell>
                                    <TableCell align="right">
                                        <Box display="flex" justifyContent="end">
                                            <Button
                                                component={RouterLink}
                                                to={`/employe/${row.id}`}
                                                size="small"
                                                color="primary"
                                                variant="contained"
                                            >
                                                View
                                            </Button>
                                            {onEdit && (
                                                <Button
                                                    component={RouterLink}
                                                    to={`/employe/edit/${row.id}`}
                                                    size="small"
                                                    color="warning"
                                                    variant="contained"
                                                    sx={{ ml: 2 }}
                                                >
                                                    edit
                                                </Button>
                                            )}
                                        </Box>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
}
