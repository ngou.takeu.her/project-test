import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useMutation, useQueryClient } from 'react-query';
import { toast } from 'react-toastify';
import { Box, Button, FormHelperText, Grid, InputLabel, OutlinedInput, Stack } from '@mui/material';
import { Formik } from 'formik';
import Loader from 'components/Loader';
import AnimateButton from 'components/@extended/AnimateButton';
import { useAuthHeader } from 'react-auth-kit';
import { postPatchAxios } from 'components/axios';
import { useNavigate } from 'react-router';

export default function EmployeModel({ defaultValues, method }) {
    const schemaEmployeCreate = yup.object().shape({
        name: yup.string().required('Nous avons besoin de votre nom').max(150),
        surname: yup.string().required('Nous avons besoin de votre prenom').max(150)
    });

    const queryClient = useQueryClient();
    const auth = useAuthHeader();
    const navigate = useNavigate();

    const { mutate, isLoading } = useMutation(
        async (data) => {
            let response;
            if (method.method === 'post') {
                response = await toast.promise(postPatchAxios('/api/employes/', data, auth()), {
                    pending: 'Creation en cours ...',
                    success: "L'employe a ete bien cree",
                    error: 'Une errreur est survenue lors de la creation'
                });
            } else {
                response = await toast.promise(postPatchAxios(`/api/employes/${method.id}/`, data, auth(), 'PATCH'), {
                    pending: 'Modification du employe en cours',
                    success: 'Le Employe a ete bien modifier',
                    error: 'Une erreur est survenue lors de la creation de la propierte'
                });
            }
            return response;
        },
        {
            onSuccess: (data) => {
                if (data.isSuccess) {
                    queryClient.invalidateQueries(['employe_index']);
                    queryClient.invalidateQueries(['employe_id']);
                    navigate(`/employes/`, { replace: true });
                }
            }
        }
    );

    return (
        <Box>
            {isLoading && <Loader />}
            <Formik initialValues={defaultValues} validationSchema={schemaEmployeCreate} onSubmit={mutate}>
                {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
                    <form noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Stack spacing={1}>
                                    <InputLabel htmlFor="name">Nom</InputLabel>
                                    <OutlinedInput
                                        id="name"
                                        type="text"
                                        name="name"
                                        value={values.name}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        placeholder="Enter votre nom"
                                        fullWidth
                                        error={Boolean(touched.name && errors.name)}
                                    />
                                    {touched.name && errors.name && (
                                        <FormHelperText error id="standard-weight-helper-text-name-login">
                                            {errors.name}
                                        </FormHelperText>
                                    )}
                                </Stack>
                            </Grid>
                            <Grid item xs={12}>
                                <Stack spacing={1}>
                                    <InputLabel htmlFor="surname">Prenom</InputLabel>
                                    <OutlinedInput
                                        id="surname"
                                        type="text"
                                        name="surname"
                                        value={values.surname}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        placeholder="Enter votre prenom"
                                        fullWidth
                                        error={Boolean(touched.surname && errors.surname)}
                                    />
                                    {touched.surname && errors.surname && (
                                        <FormHelperText error id="standard-weight-helper-text-surname-login">
                                            {errors.surname}
                                        </FormHelperText>
                                    )}
                                </Stack>
                            </Grid>
                            <Grid item mt={4} xs={12}>
                                <AnimateButton>
                                    <Button
                                        disableElevation
                                        disabled={isLoading}
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                    >
                                        {method?.method === 'patch' ? 'Editer' : 'Creer'}
                                    </Button>
                                </AnimateButton>
                            </Grid>
                        </Grid>
                    </form>
                )}
            </Formik>
        </Box>
    );
}
