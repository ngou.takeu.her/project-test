import { Box, Typography } from '@mui/material';
import { postPatchAxios } from 'components/axios';
import { loadAxios } from 'components/axios';
import Loader from 'components/Loader';
import MainCard from 'components/MainCard';
import { useCallback, useEffect, useState } from 'react';
import { useAuthHeader } from 'react-auth-kit';
import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router';
import EmployeModel from './ModelEmploye';

function EditEmploye() {
    const [defaultValues, setDefaultValues] = useState({});
    const { id } = useParams();
    const authHeader = useAuthHeader();
    const navigate = useNavigate();
    const [onEdit, setOnEdit] = useState(false);

    const handleChangeOnEdit = useCallback((statut) => {
        if (statut !== 403) {
            setOnEdit(true);
        } else if (statut === 403) {
            navigate('/403');
        }
    });

    const _onEdit = () => {
        postPatchAxios('api/employes/0/', {}, authHeader(), 'patch', handleChangeOnEdit);
        return;
    };

    _onEdit();

    const { data, isLoading, isError } = useQuery(['employe_id'], () => loadAxios(`api/employes/${id}/`, authHeader(), navigate));
    const employe = data || [];

    useEffect(() => {
        if (employe && employe.id) {
            setDefaultValues({
                name: employe?.name,
                surname: employe?.surname
            });
        }
    }, [employe]);

    const method = {
        method: 'patch',
        id
    };

    return (
        <Box>
            {isLoading && onEdit ? (
                <Loader />
            ) : isError ? (
                <Box>
                    <Typography variant="h4">Error fecth data</Typography>
                </Box>
            ) : (
                onEdit &&
                employe &&
                employe.name === defaultValues.name && (
                    <MainCard title={`Editer le employe Id : ${id}`}>
                        <EmployeModel defaultValues={defaultValues} method={method} />
                    </MainCard>
                )
            )}
        </Box>
    );
}

export default EditEmploye;
