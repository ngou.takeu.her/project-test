import { Box, Typography } from '@mui/material';
import MainCard from 'components/MainCard';

export default function Error_403() {
    return (
        <MainCard>
            <Box display="flex" justifyContent="center">
                <Box textAlign="center">
                    <Typography variant="h1">403</Typography>
                    <Typography mt={3} variant="h3">
                        Vous avez pas le droit d'acces
                    </Typography>
                </Box>
            </Box>
        </MainCard>
    );
}
