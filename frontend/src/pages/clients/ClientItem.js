import MainCard from "components/MainCard";
import { useQuery } from "react-query";

export default function ClientItem() {
    const { data, isLoading, isError } = useQuery(['client_id'], () => loadAxios(`api/clients/${id}/`, authHeader()));
    const client = data || [];
    return (
        <MainCard title={`Client id : ${client.id}`}>
            
        </MainCard>
    )
}