import { Box, Typography } from '@mui/material';
import { postPatchAxios } from 'components/axios';
import { loadAxios } from 'components/axios';
import Loader from 'components/Loader';
import MainCard from 'components/MainCard';
import { useCallback, useEffect, useState } from 'react';
import { useAuthHeader } from 'react-auth-kit';
import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router';
import ClientModel from './ModelClient';

function EditClient() {
    const [defaultValues, setDefaultValues] = useState({});
    const { id } = useParams();
    const authHeader = useAuthHeader();
    const navigate = useNavigate();
    const [onEdit, setOnEdit] = useState(false);

    const handleChangeOnEdit = useCallback((statut) => {
        if (statut !== 403) {
            setOnEdit(true);
        } else if (statut === 403) {
            navigate('/403');
        }
    });

    const _onEdit = () => {
        postPatchAxios('api/clients/0/', {}, authHeader(), 'patch', handleChangeOnEdit);
        return;
    };

    _onEdit();

    const { data, isLoading, isError } = useQuery(['client_id'], () => loadAxios(`api/clients/${id}/`, authHeader(), navigate));
    const client = data || [];

    useEffect(() => {
        if (client && client.id) {
            setDefaultValues({
                name: client?.name,
                surname: client?.surname
            });
        }
    }, [client]);

    const method = {
        method: 'patch',
        id
    };

    return (
        <Box>
            {isLoading && onEdit ? (
                <Loader />
            ) : isError ? (
                <Box>
                    <Typography variant="h4">Error fecth data</Typography>
                </Box>
            ) : (
                onEdit &&
                client &&
                client.name === defaultValues.name && (
                    <MainCard title={`Editer le client Id : ${id}`}>
                        <ClientModel defaultValues={defaultValues} method={method} />
                    </MainCard>
                )
            )}
        </Box>
    );
}

export default EditClient;
