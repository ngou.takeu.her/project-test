import MainCard from 'components/MainCard';
import ClientModel from './ModelClient';

function CreateClient() {
    const defaultValues = {
        name: '',
        surname: ''
    };

    const method = {
        method: 'post',
        id: null
    };

    return (
        <MainCard>
            <ClientModel defaultValues={defaultValues} method={method} />
        </MainCard>
    );
}

export default CreateClient;
