import axios from 'axios';

const dev = 'http://0.0.0.0:8000/';
// const dev = "/fipro_api/public/api";
const api = axios.create({
    baseURL: dev,
    responseType: 'json',
    headers: {
        Accept: 'application/json'
    }
});

export async function loadAxios(url, token, callback) {
    const response = await api({
        url,
        headers: {
            Authorization: token
        }
    })
        .then(({ data }) => data)
        .catch((e) => {
            if (e.response.status == 403) {
                return callback('/403');
            }
            throw new Error(e);
        });
    return response;
}

export async function postAxios(url, dataPost) {
    const response = await api
        .post(url, dataPost)
        .then(({ data }) => ({
            data,
            isSuccess: true
        }))
        .catch((e) => {
            throw new Error(e.response.status);
        });
    console.log(response);
    return response;
}

export async function postPatchAxios(url, dataPost, token, method = 'post', callback) {
    return await api({
        url,
        headers: {
            Authorization: token
        },
        data: dataPost,
        method
    })
        .then(({ data }) => ({
            data,
            isSuccess: true
        }))
        .catch((e) => {
            if (e.response.status == 403) {
                return callback(403);
            }
            return callback(e.response.statut);
        });
}

export async function deleteAxios(url) {
    const response = await api({
        method: 'DELETE',
        url
    })
        .then(() => ({
            isSuccess: true
        }))
        .catch((e) => {
            throw new Error(e);
        });
    return response;
}
