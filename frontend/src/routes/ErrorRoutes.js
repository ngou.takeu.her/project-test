import Loadable from 'components/Loadable';
import MinimalLayout from 'layout/MinimalLayout';
import { lazy } from 'react';
import { Navigate } from 'react-router';

const Error_404 = Loadable(lazy(() => import('pages/404')));

const ErrorRoutes = {
    path: '/',
    element: <MinimalLayout />,
    children: [
        {
            path: '*',
            element: <Navigate replace element={<Error_404 />} />
        }
    ]
};

export default ErrorRoutes;
