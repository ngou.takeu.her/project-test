import Loadable from 'components/Loadable';
import { lazy } from 'react';
import { Navigate, useRoutes, Routes, Route } from 'react-router-dom';
import ErrorRoutes from './ErrorRoutes';
import MainLayout from 'layout/MainLayout';

// project import
import LoginRoutes from './LoginRoutes';
import MainRoutes from './MainRoutes';
import { RequireAuth } from 'react-auth-kit';
const DashboardDefault = Loadable(lazy(() => import('pages/dashboard')));
const Error_404 = Loadable(lazy(() => import('pages/404')));
const Error_403 = Loadable(lazy(() => import('pages/403')));

const Client = Loadable(lazy(() => import('pages/clients')));
const ClientCreate = Loadable(lazy(() => import('pages/clients/CreateClient')));
const ClientEdit = Loadable(lazy(() => import('pages/clients/EditClient')));
const Employe = Loadable(lazy(() => import('pages/employe')));
const EmployeCreate = Loadable(lazy(() => import('pages/employe/CreateEmploye')));
const EmployeEdit = Loadable(lazy(() => import('pages/employe/EditEmploye')));

const AuthLogin = Loadable(lazy(() => import('pages/authentication/Login')));

// ==============================|| ROUTING RENDER ||============================== //

// export default function ThemeRoutes() {
//     return useRoutes([MainRoutes, LoginRoutes]);
// }

function AllRoutes() {
    return (
        <Routes>
            <Route path="/" exact element={<Navigate replace to="/dashboard" />} />
            <Route path="*" element={<Navigate replace to="/404" />} />
            <Route path="/">
                <Route
                    path="/"
                    element={
                        <RequireAuth loginPath="/login">
                            <MainLayout />
                        </RequireAuth>
                    }
                >
                    <Route path="dashboard" element={<DashboardDefault />} />
                    <Route path="clients" name="client_index" element={<Client />} />
                    <Route path="client/add" name="client_create" element={<ClientCreate />} />
                    <Route path="client/edit/:id" name="client_create" element={<ClientEdit />} />
                    <Route path="employes" name="employe_index" element={<Employe />} />
                    <Route path="employe/add" name="employe_create" element={<EmployeCreate />} />
                    <Route path="employe/edit/:id" name="employe_edit" element={<EmployeEdit />} />
                    <Route path="403" name="fobiden" element={<Error_403 />} />
                </Route>
                <Route path="login" name="login" element={<AuthLogin />} />
                <Route path="404" element={<Error_404 />} />
            </Route>
        </Routes>
    );
}

export default AllRoutes;
