// assets
import { LoginOutlined, ProfileOutlined, UserOutlined } from '@ant-design/icons';

// icons
const icons = {
    LoginOutlined,
    ProfileOutlined,
    UserOutlined
};

// ==============================|| MENU ITEMS - EXTRA PAGES ||============================== //

const pages = {
    id: 'authentication',
    title: 'Groupe client',
    type: 'group',
    children: [
        {
            id: 'client',
            title: 'Client',
            type: 'item',
            url: '/clients',
            icon: icons.UserOutlined
        },
        {
            id: 'employe',
            title: 'Employes',
            type: 'item',
            url: '/employes',
            icon: icons.UserOutlined
        }
    ]
};

export default pages;
